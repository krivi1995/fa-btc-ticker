import arena_helper as arena
import threading
import time
import fileinput
import logging

#supported crypto exchange server tickers
crypto_server_url_list =[
    'http://127.0.0.1:5002/bitstamp/ticker',
    'http://127.0.0.1:5003/poloniex/ticker',
    'http://127.0.0.1:5001/kraken/ticker'
]

#crypto exchanges
crypto_server_name_list =[
    'Bitstamp',
    'Poloniex',
    'Kraken'
]

#supported exchanges for trading
trading_marketplaces_list = [
    'POLONIEX_SPOT'
]

#supported currencies
supported_currencies_list = ['BTC', 'XRP', 'ETH', 'EOS', 'LTC', 'BCH']

#creates logger
def get_logger(name):
    log_format = '%(asctime)s  %(name)8s  %(levelname)5s  %(message)s'
    logging.basicConfig(level=logging.DEBUG,
                        format=log_format,
                        filename='crypto_engine.log',
                        filemode='a')
    return logging.getLogger(name)

#listens for commands(stoping ticker, ticker update interval,...)
class CommandListener(threading.Thread):
    def __init__(self, ticker):
        threading.Thread.__init__(self)
        self.running = True
        self.ticker = ticker
        self.logger = get_logger('command')

    def run(self):
        self.logger.info('Started command listener.')
        while self.running:
            for command in fileinput.input():
                if 'stop' in command.lower():
                    self.running = False
                    if self.ticker is not None:
                        self.ticker.running = False
                    print 'Stopping ticker...'
                    self.logger.info('Stopping ticker...')
                    break
                if 'interval' in command.lower():
                    for word in command.split():
                        if word.isdigit():
                            self.ticker.interval = int(word)
                            print 'Changed update interval.'
                            self.logger.info('Changed update interval.')


#wrapper wor interacting between crypto exchange servers and Front Arena(ADS)
class Ticker(threading.Thread):
    def __init__(self, interval):
        threading.Thread.__init__(self)
        self.interval = interval
        self.arena = arena.FrontArena(crypto_server_url_list,
                                     crypto_server_name_list,
                                     supported_currencies_list,
                                     trading_marketplaces_list
                                     )
        self.running = True
        self.logger = get_logger('ticker')

    def run(self):
        self.logger.info('Started crypto currency ticker.')
        while self.running:
            self.logger.info('Updating crypto currencies prices...')
            print('Updating prices...')
            self.arena.ticker_update()
            self.logger.info('Updating crypto currencies prices...')
            print('Finished updating prices.')
            time.sleep(self.interval)
        self.arena.unsubscribe_from_trades()
        self.logger.info('Stopped crypto currency ticker.')


ticker = Ticker(10)
command_listener = CommandListener(ticker)

ticker.start()
command_listener.start()



