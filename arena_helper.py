import requests
import json
import urllib
import acm
import ael
import logging

# callback function that is called fron Front Arena every time a new trade for cupported_currencies is created
# it calls Poloniex Server API for placing bys and sell orders
def crypto_trade_callback(obj, trade, arg, event):
    if trade is not None and event=='insert':
        trade = acm.FTrade[trade.trdnbr]
        if trade.Market() is None:
            return
        if trade.Market().Name() == 'POLONIEX_SPOT':
            url = None
            url_buy = 'http://127.0.0.1:5003/poloniex/buy'
            url_sell = 'http://127.0.0.1:5003/poloniex/sell'
            currency = trade.Instrument().Name()
            price = trade.Price()
            amount = trade.Quantity()
            direction = trade.Direction()
            if direction == 'Buy':
                url = url_buy
            else:
                url = url_sell
                amount = abs(amount)
            params = {
                'currency': currency.lower(),
                'price': price,
                'amount': amount
            }
            #response = requests.get(url, params) # get request
            response = requests.post(url, params)
            if response.ok:
                response_data = json.loads(response.content)
                print 'Order has been sent to Crypto Exchange'
                print response_data


#creates logger
def get_logger(name):
    log_format = '%(asctime)s  %(name)8s  %(levelname)5s  %(message)s'
    logging.basicConfig(level=logging.DEBUG,
                        format=log_format,
                        filename='crypto_engine.log',
                        filemode='a')
    return logging.getLogger(name)


# wrapper class for interacting with Front Arena
# initializes ADS for working with crypto
# handles call to crypto servers for ticker info, updates price in ADS, subscribes to trades,...
class FrontArena:

    def __init__(self,
                 crypto_server_url,
                 crypto_server_name_list,
                 supported_currencies_list,
                 trading_marketplaces_list
                 ):
        self.logger = get_logger('front_arena')
        self.logger.info('Connecting to ADS...')
        self.connect_to_prime()
        self.crypto_server_url = crypto_server_url
        self.crypto_server_name_list = crypto_server_name_list
        self.supported_currencies_list = supported_currencies_list
        self.trading_marketplaces_list = trading_marketplaces_list
        self.logger.info('Initializing data for working with crypto...')
        self.init_arena_crypto_data()
        self.currencies = []
        for currency in supported_currencies_list:
            self.currencies.append(acm.FStock[currency])

    def connect_to_prime(self):
        try:
            acm.Connect('localhost:9000', 'arenasys', 'front2back', '')
        except:
            self.logger.error('Cannot connect to ADS')

    #creates all neccessary data in Front arena for working with crypto currencies:
    #crypto currencies as stock (supported_currencies_list)
    #crypto exchange markets that provides price data (crypto_server_name_list)
    #subscribes callback function(crypto_trade_callback) to crypto currency trades(using ael)
    def init_arena_crypto_data(self):
        for currency_name in self.supported_currencies_list:
            currency = acm.FStock[currency_name]
            if currency is None:
                self.create_cryptocurrency(currency_name)
        for marker_name in self.crypto_server_name_list:
            market = acm.FCounterParty[marker_name]
            if market is None:
                self.create_crypto_market(marker_name)
        for market_name in self.trading_marketplaces_list:
            market = acm.FMarketPlace[market_name]
            if market is None:
                self.create_marketplace(market_name)
        self.subscribe_to_trades()

    #creates a new crypto currency in Front Arena, as a stock instrument, if provided currency does not already exist
    def create_cryptocurrency(self, name):
        self.logger.info('Creating new crypto currency - %s'%name)
        stock = acm.FStock()
        stock.Name(name)
        stock.Commit()
        return stock

    #creates new market in Front Arena if provided market does not already exists
    def create_crypto_market(self, name):
        self.logger.info('Creating new crypto market data provider - %s'%name)
        market = acm.FCounterParty()
        market.Name(name)
        market.Type(5)
        market.Commit()
        return market

    #creates new marketplace for trading in Front Arena if provided market does not already exists
    def create_marketplace(self, name):
        self.logger.info('Creating new crypto marketplace for trades- %s' % name)
        market = acm.FMarketPlace()
        market.Name(name)
        market.Commit()
        return market

    #creates new price in lates table
    def create_price(self, market, currency):
        self.logger.info('Creating new price for - %s' % currency)
        price = acm.FPrice()
        price.Currency('USD')
        price.Market(market)
        price.Instrument(currency.Name())
        price.Day(acm.Time.DateNow())
        price.Commit()
        return price

    #subscribint to crypto currency trades (using ael)
    def subscribe_to_trades(self):
        self.logger.info('Creating subscription to trades')
        for currency_name in self.supported_currencies_list:
            instrument = ael.Instrument[currency_name]
            trade_sel = instrument.trades()
            trade_sel.subscribe(crypto_trade_callback)

    #unsubscribint from crypto currency trades (using ael)
    def unsubscribe_from_trades(self):
        self.logger.info('Usubscribing from trades')
        for currency_name in self.supported_currencies_list:
            instrument = ael.Instrument[currency_name]
            trade_sel = instrument.trades()
            trade_sel.unsubscribe(crypto_trade_callback)

    #updates given price with data from crypto exchange
    def update_price(self, price, data):
        try:
            if data['ask']:
                price.Ask(float(data['ask']))
            if data['bid']:
                price.Bid(float(data['bid']))
            if data['high']:
                price.High(float(data['high']))
            if data['low']:
                price.Low(float(data['low']))
            #if data['open']:
                #price.Open(float(data['open']))
            if data['volume']:
                price.TotalVolume(float(data['volume']))
            price.Day(acm.Time.DateNow())
            price.Commit()
        except:
            self.logger.error('Cannot update price.')

    #if thre are not prices in lates table it will create new prices for given exchanges
    #updates existing prices in latest table
    def update_crypto_price(self, data, exchange, currency):
        try:
            latest_prices = currency.Prices()
            if len(latest_prices) == 0:                             #there are no prices for today, for selected currency
                for crypto_server in self.crypto_server_name_list:  #creating price data from evry available crypto market
                    self.create_price(crypto_server, currency)
                latest_prices = currency.Prices()
            for price in latest_prices:                             #updating prices for selected crypto currency
                crypto_exchange = price.Market().Id()
                if crypto_exchange == exchange:
                    self.update_price(price, data)
        except:
            self.logger.error('Cannot update %s price.'%currency.Name())

    #for every crypto currency (stock instrumen in currencies list) updates prices:
    #each currency's  price is updated for each existing crypto market server
    def ticker_update(self):
        for currency in self.currencies:
            self.logger.info('Updating prices for %s'%currency.Name())
            for exchange, api in zip(self.crypto_server_name_list, self.crypto_server_url):
                self.ticker(exchange, api, currency)

    def ticker(self, exchange, url, currency):
        try:
            params = {'currency': currency.Name()}
            response = requests.get(url, params)
            if response.ok:
                ticker_data = {}
                response_data = json.loads(response.content)
                for key in response_data:
                    if key == 'msg':
                        return
                    ticker_data[key] = response_data[key]
                self.update_crypto_price(ticker_data, exchange, currency)
        except:
            self.logger.error('Cannot get data from servers.')



